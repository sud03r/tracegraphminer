SRC_DIR = ./src
BIN_DIR = ./bin
INCLUDES := $(wildcard ./include/*)
OGDFDIR = /home/n26kumar/projects/ogdf
INCLUDE = -I$(OGDFDIR)/include/ -I./include/
DEPS = $(OGDFDIR)/_debug/libOGDF.a -lpthread
CC = g++ -std=c++0x -DOGDF_DEBUG
DOT2PNG = /home/n26kumar/software/bin/dot2png
OBJECTS := $(subst src,bin,$(patsubst %.cc,%.o,$(wildcard $(SRC_DIR)/*.cc)))

all : genPatterns

$(BIN_DIR)/%.o : $(SRC_DIR)/%.cc $(INCLUDES)
	$(CC) $(INCLUDE) -c -g -o $@ $<

genPatterns : $(OBJECTS)
	$(CC) $^ -o $@ $(DEPS)

generateImages :
	mkdir -p generated-patterns/images
	$(DOT2PNG) generated-patterns/ generated-patterns/images

clean :
	rm -rf generated-patterns/*

distclean :
	rm -rf *.svg bin/* genPatterns generated-patterns/*
.PHONY: clean generateImages

#include "TraceGraph.h"
#include <ogdf/fileformats/GraphIO.h>

#ifndef _MAXIMAL_PATTERN_H
#define _MAXIMAL_PATTERN_H

class MaximalPattern {
	TraceGraph& m_Graph; // Underlying graph
	List<edge> m_Edges; // Edges in pattern
	edge m_startEdge;
	NodeArray<bool> m_Visited;

public:
	// A maximal pattern starting at edge e
	MaximalPattern(edge e, TraceGraph& tg) : m_Graph(tg), m_Visited(tg, false) {
		m_Edges.pushBack(e);
		m_startEdge = e;
	}

	edge hostEdge() const {
		return m_startEdge;
	}

	int size() const {
		return m_Edges.size();
	}

	const List<edge>& edges() const {
		return m_Edges;
	}
	void add(edge e) {
		if (e != m_Edges.front())
			m_Edges.pushBack(e);
	}

	// Starting with an edge we grow as far as possible 
	// in depth first order
	void growDFS(node v, List<TS*>& prevEdgeMappings) {
		// If node v has indegree 1, then I can re-use the previously
		// created maxPattern
		// TODO

#ifdef DBG
		cout << "Carried mappings at " << m_Graph.label(v) << ":" << endl;
		for (TS* ts : prevEdgeMappings) {
			ts->Pr();
		}
		cout << endl;
#endif
		
		if (m_Visited[v])
			return;
		
		m_Visited[v] = true;

		EdgeArray<List<TS*> > nextEdgeMappings(m_Graph);
		for (TS* ts :  prevEdgeMappings) {
			edge e = ts->nextEdge();
			if (e) {
				nextEdgeMappings[e].pushBack(ts->nextTS());
				OGDF_ASSERT(ts->nextTS()->theEdge() == e);
			}
		}

		edge e;
		forall_adj_edges(e, v) {
			if (e->source() != v)
				continue;
			List<TS*> edgeMapping = nextEdgeMappings[e];
			if (edgeMapping.size() >= m_Graph.threshold()) {
				add(e);
				node w = e->opposite(v);
				growDFS(w, edgeMapping);
			}
		}
	}
	
	void Pr() {
		for (edge e : m_Edges)
			cout << m_Graph.printEdge(e);
		cout << endl;
	}

	void sort() {
		m_Edges.quicksort();
	}
};

namespace ogdf {
	OGDF_STD_COMPARER(edge);
	template<> class StdComparer<MaximalPattern const*>
	{
		public:
			static bool less   (const MaximalPattern* &x, const MaximalPattern* &y) { return x->size() <  y->size(); }
			static bool leq    (const MaximalPattern* &x, const MaximalPattern* &y) { return x->size() <= y->size(); }
			static bool greater(const MaximalPattern* &x, const MaximalPattern* &y) { return x->size() >  y->size(); }
			static bool geq    (const MaximalPattern* &x, const MaximalPattern* &y) { return x->size() >= y->size(); }
			static bool equal  (const MaximalPattern* &x, const MaximalPattern* &y) { return x->size() == y->size(); }
	};
}

template class EdgeArray<MaximalPattern*>;
// Required to display the patterns

class PatternGraph : public Graph {
	GraphAttributes m_GA;
	NodeArray<node> m_Orig2New;
public:
	PatternGraph(const List<node>& pNodes, TraceGraph& origGraph) : 
									m_GA(*this, 
									GraphAttributes::nodeLabel | GraphAttributes::edgeLabel), 
									m_Orig2New(origGraph)
	{
		for (node pn : pNodes) {
			node n = newNode();
			m_Orig2New[pn] = n;
			m_GA.label(n) = origGraph.label(pn);
		}

		for (node pn : pNodes) {
			edge pe;
			forall_adj_edges(pe, pn) {
				node src = m_Orig2New[pe->source()];
				node target = m_Orig2New[pe->target()];
				if (!searchEdge(src, target))
					newEdge(m_Orig2New[pe->source()], m_Orig2New[pe->target()]);
			}
		}

	}

	PatternGraph(const List<node>& pNodes, const List<edge>& pEdges, TraceGraph& origGraph) : 
									m_GA(*this, 
									GraphAttributes::nodeLabel | GraphAttributes::edgeLabel), 
									m_Orig2New(origGraph)
	{
		for (node pn : pNodes) {
			node n = newNode();
			m_Orig2New[pn] = n;
			m_GA.label(n) = origGraph.label(pn);
		}

		for (edge pe : pEdges) {
			node src = m_Orig2New[pe->source()];
			node target = m_Orig2New[pe->target()];
			if (!searchEdge(src, target))
				newEdge(m_Orig2New[pe->source()], m_Orig2New[pe->target()]);
		}

	}

	void writeGraph(const char* fName) {
		GraphIO::writeDOT(m_GA, fName);
	}

};

#endif

